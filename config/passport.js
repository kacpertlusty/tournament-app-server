const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');

const User = mongoose.model('User');

// const opts = {};
// opts.jwtFromRequest = ExtractJWT.fromAuthHeaderAsBearerToken();
// opts.secretOrKey = 'secret';
// opts.issuer = 'accounts.examplesoft.com';
// opts.audience = 'yoursite.net';

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password',
}, (email, password, done) => {
  User.findOne({ email })
    .then((user) => {
      if (!user || !user.validatePassword(password)) {
        return done(null, false, { errors: { 'email or password': 'is invalid' } });
      }
      return done(null, user);
    });
}));

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'secret',
},
  ((jwtPayload, cb) => User.findById(jwtPayload.id)
    .then(user => cb(null, user))
    .catch(err => cb(err))
  )));

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user);
  });
});

passport.loginRequired = (req, res, next) => {
  if (!req.user) return res.redirect('/users/login');
  return next();
};

passport.checkToken = (req, res, next) => {
  (passport.authenticate('jwt', { session: false }, (err, user, info) => {
    if (err) { return next(err); }
    if (!user) {
      if (info.name === 'TokenExpiredError') {
        return res.status(401).json({ message: 'Your token has expired.' });
      }
      return res.status(401).json({ message: info.message });
    }
    req.user = user;
    return next();
  }))(req, res, next);
};

module.exports = passport;
