module.exports = {
  factorK: 16,
  swiss: {
    doubleMatchFactor: 400,
    pointDifferenceRoot: 2,
  },
};
