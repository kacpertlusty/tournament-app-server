const isInRange = (a, min, max) => a >= min && a <= max

module.exports = {
  handleError (res, err, code) {
    if (err instanceof Error) {
      return res.status(code || 400).json({
        error: err.message
      })
    }
    return res.status(code || 400).json(err)
  },

  validateScore (a = 0, b = 0) {
    const aNum = Number(a)
    const bNum = Number(b)
    if (aNum + bNum !== 20) return false
    if (!isInRange(aNum, 0, 20)) return false
    return isInRange(bNum, 0, 20)
  }
}
