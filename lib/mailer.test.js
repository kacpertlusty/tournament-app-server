const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

const { expect } = chai;
chai.use(chaiAsPromised);

const Mailer = require('./mailer');

describe('mailer', () => {
  context('sendWelcomeEmail', () => {
    const mailer = new Mailer();
    it('should check for name and email', () => {
      expect(mailer.sendWelcomeEmail()).to.eventually.be.rejectedWith('Invalid input');
      expect(mailer.sendWelcomeEmail('foo@bar')).to.eventually.be.rejectedWith('Invalid input');
      expect(mailer.sendWelcomeEmail(null, 'foo')).to.eventually.be.rejectedWith('Invalid input');
    });

    it('should return mail body', () => {
      expect(mailer.sendWelcomeEmail('foo@bar.com', 'foo', 'bar')).to.eventually.be.equal('Dear foo bar, welcome to our family!');
    });
  });
});
