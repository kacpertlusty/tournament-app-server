const config = require('../config/config')

const single = (history = [], players = [], currentSetup = [], globalcurrent = []) => {
  const current = [...currentSetup]
  if (players.length === 0) {
    return globalcurrent.push([...currentSetup])
  }
  if (players.length === 1) {
    const player = players.pop()
    return globalcurrent.push([...current, { playerA: { player, score: player.score }, playerB: 'BYE' }])
  }
  const firstplayer = players.pop()
  return players.forEach((player) => {
    const pairing = { playerA: firstplayer, playerB: player }
    const filteredPlayers = players.filter(p => p.player.id !== player.player.id)
    single(history, filteredPlayers, [...current, pairing], globalcurrent)
  })
}

const filter = (m, nm) => {
  const aId = nm.playerA.id
  const bId = nm.playerB.id
  return m.playerA.player.id === aId && m.playerB.player.id === bId
}

const swiss = (history = [], players = [], currentSetup = []) => {
  const { doubleMatchFactor, pointDifferenceRoot } = config.swiss
  const pairings = []
  single(history, [...players], currentSetup, pairings)
  return pairings.map((pairing) => {
    let cost = 0
    pairing.forEach((newMatch) => {
      cost += history.filter(match => filter(match, newMatch)).length * doubleMatchFactor
      cost += (newMatch.playerA.score - newMatch.playerB.score) ** pointDifferenceRoot
    })
    return { pairing, cost }
  })
}

module.exports = swiss
