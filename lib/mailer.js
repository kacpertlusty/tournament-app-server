module.exports = class Mailer {
  // eslint-disable-next-line class-methods-use-this
  sendWelcomeEmail(email, name, lastName) {
    if (!email || !name || !lastName) {
      return Promise.reject(new Error('Invalid input'));
    }

    const body = `Dear ${name} ${lastName}, welcome to our family!`;

    return Promise.resolve(body);
  }
};
