const mongoose = require('mongoose')
const Match = require('./match')
const Detachment = require('./detachment')
const swiss = require('../lib/swiss')

const Round = mongoose.Schema({
  matches: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Match' }],
  turn: { type: Number, require: true },
  finished: { type: Boolean, default: false }
})

const PlayerScore = mongoose.Schema({
  player: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  score: { type: Number, default: 0 },
  detachments: { type: [Detachment], default: [] },
  roster: { type: String, default: '' },
  rosterUpdateTime: { type: String, required: false },
  bonusScore: { type: Number, default: 0 }
})

const TournamentSchema = mongoose.Schema({
  name: { type: String, required: true, unique: true },
  city: { type: String, required: true },
  zipCode: { type: String, required: true },
  street: { type: String, required: true },
  creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
  maxPlayers: { type: Number, required: true },
  date: { type: Date, required: true },
  maxRounds: { type: Number, required: true },
  description: { type: String },
  rules: { type: String },
  players: { type: [PlayerScore], default: [] },
  rounds: { type: [Round] },
  finished: { type: Boolean, default: false },
  rank: { type: String, default: 'Lokalny' },
  accepted: { type: Boolean, default: false },
  detachmentLimit: { type: Number, required: false, default: 3 },
  rosterTimeLimit: { type: Date, required: true },
  penaltyForLateRoster: { type: Number, required: false, default: 10 }
})

TournamentSchema.methods.registerUser = function registerUser (user, data, cb) {
  if (this.players.filter(p => p.player._id.equals(user.id)).length > 0) {
    cb(new Error('User already registered'))
    return
  }
  const { detachments, roster } = data
  this.players.push({
    player: user,
    score: 0,
    roster,
    detachments
  })
  user.tournaments.push(this._id)
  user.save()
  this.save().then(value => cb(null, value), err => cb(err, null))
}

TournamentSchema.methods.createFirstRound = function createFirstRound (cb) {
  const round = {
    matches: [],
    turn: 1,
    finished: false
  }
  const sortedPlayers = this.players.sort((a, b) => {
    if (a.player.mmr > b.player.mmr) return -1
    return 1
  })
  for (let i = 0; i < (sortedPlayers.length); i += 2) {
    const playerA = {
      player: sortedPlayers[i].player,
      detachments: sortedPlayers[i].detachments
    }
    const playerB = {
      player: (sortedPlayers[i + 1] || {}).player,
      detachments: (sortedPlayers[i + 1] || {}).detachments
    }
    const matchArgs = {
      playerA,
      playerB,
      table: i / 2 + 1,
      tournament: this._id
    }
    if (!playerB.player) {
      matchArgs.playerA.score = 13
      matchArgs.finished = true
    }

    const match = new Match(matchArgs)
    round.matches.push(match)
    match.save()
  }
  this.rounds.push(round)
  this.save().then(value => cb(null, value), err => cb(err, null)).catch(err => cb(err, null))
}

TournamentSchema.methods.finishRound = function finishRound (roundToFinish, cb) {
  const round = roundToFinish
  round.finished = true
  round.matches.forEach((m) => {
    if (!m.finished) m.finishGame(0, 0, this)
    this.players.find(p => p.player.id === m.playerA.player.id).score += m.playerA.score
    this.players.find(p => p.player.id === m.playerB.player.id).score += m.playerB.score
  })
  this.save().then(obj => cb(null, obj), err => cb(err, null)).catch(err => cb(err, null))
}

TournamentSchema.methods.createRound = function createRound (cb) {
  const round = {
    turn: this.rounds.length + 1
  }
  const sortedResults = this.players.sort((a, b) => {
    if (a.score > b.score) return -1
    return 1
  })

  round.matches = []
  let history = []
  this.rounds.forEach((r) => {
    history = [...history, ...r.matches]
  })
  if (sortedResults % 2) {
    const byePlayer = sortedResults.pop()
    round.matches.push({
      player: byePlayer.player,
      detachments: byePlayer.detachments
    })
  }
  const { pairing } = swiss(history, sortedResults.filter(() => true)).sort((a, b) => a - b)[0]
  for (let i = 0; i < pairing.length; i += 1) {
    const matchArgs = {
      playerA: {
        player: pairing[i].playerA.player,
        detachments: pairing[i].playerA.detachments
      },
      playerB: {
        player: pairing[i].playerB.player,
        detachments: pairing[i].playerB.detachments
      },
      tournament: this._id,
      table: i
    }
    const match = new Match(matchArgs)
    round.matches.push(match)
    match.save()
  }
  this.rounds.push(round)
  this.save().then(() => cb(null, this), err => cb(err, null)).catch(err => cb(err, null))
}

TournamentSchema.methods.finish = function finishTournament (cb) {
  const results = this.players.sort((a, b) => {
    if (a.score > b.score) return -1
    return 1
  })
  this.finished = true
  // eslint-disable-next-line no-unused-vars
  return this.save((err, _res) => {
    if (err) return cb(err, null)
    return cb(null, results)
  })
}

TournamentSchema.methods.unregister = function unregisterPlayer (player, cb) {
  if (!player) return cb(new Error('Uzytkownik musi istnieć.'), null)
  const registered = this.players.find(p => p.player.id === player.id)
  if (!registered) return cb(null, { message: 'user is not registered' })
  this.players.splice(this.players.indexOf(registered), 1)
  player.removeTournament(this._id, () => {})
  return this.save((err, result) => {
    if (result) return cb(null, { message: 'Zostałeś wyrejestrowany z turnieju.' })
    return cb(err, null)
  })
}

TournamentSchema.methods.addScore = function addScoreToPlayer (id, score, cb) {
  if (!id || !score) return cb(new Error('Nie podano wymaganych argumentów'), null)
  if (this.finished) return cb(null, { message: 'Gra juz została zakończona' })
  const player = this.players.find(p => p.player.id === id)
  if (!player) return cb(null, { message: 'Gracz nie bierze udziału w turnieju' })
  player.bonusScore = Number(player.bonusScore) + Number(score)
  return this.save((err, newTournament) => {
    if (err) return cb(err, null)
    return cb(null, newTournament)
  })
}

TournamentSchema.methods.updateScore = function updateScoreAfterModifyMatch (playerId, score, cb) {
  if (!playerId || score === undefined) return cb(new Error('Nie podano wymaganych argumentów'), null)
  const player = this.players.filter(p => p.player.id === playerId)
  if (!player.length) return cb(new Error('Brak gracza w turnieju'))
  player[0].score += score
  return this.save((err, savedTournament) => {
    if (err) return cb(err, null)
    return cb(null, savedTournament)
  })
}

module.exports = mongoose.model('Tournament', TournamentSchema)
