const mongoose = require('mongoose')
const crypto = require('crypto')
const jwt = require('jsonwebtoken')

const UserSchema = mongoose.Schema({
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  joinDate: { type: Date, required: true },
  mmr: { type: Number, default: 1500 },
  salt: { type: String },
  games: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Match' }],
  tournaments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Tournament' }]
})

UserSchema.methods.setPassword = function setPassword (password) {
  this.salt = crypto.randomBytes(16).toString('hex')
  this.password = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex')
}

UserSchema.methods.validatePassword = function validatePassword (password) {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex')
  return this.password === hash
}

UserSchema.methods.generateJWT = function generateJWT () {
  const today = new Date()
  const expirationDate = new Date(today)
  expirationDate.setDate(today.getDate() + 60)

  return jwt.sign({
    email: this.email,
    id: this._id
  }, 'secret', { expiresIn: '7d' })
}

UserSchema.methods.toAuthJSON = function toAuthJSON () {
  return {
    _id: this._id,
    email: this.email,
    token: this.generateJWT(),
    firstName: this.firstName,
    lastName: this.lastName
  }
}

UserSchema.methods.publicString = function publicString () {
  return {
    _id: this._id,
    email: this.email,
    firstName: this.firstName,
    lastName: this.lastName,
    games: this.games
  }
}

UserSchema.methods.removeTournament = function removeTournamentFromUserHistory (tournamentId) {
  const tournament = this.tournaments.find(p => p.id === tournamentId)
  if (!tournament) return
  this.tournaments.splice(
    this.tournaments.indexOf(tournament),
    1
  )
  this.save()
}

module.exports = mongoose.model('User', UserSchema)
