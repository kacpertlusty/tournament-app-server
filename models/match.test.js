/* global context, it, describe, */
const chai = require('chai');

const { expect } = chai;

const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

// const mongoose = require('mongoose');
// const rewire = require('rewire');

const User = require('./user');
const Match = require('./match');

const sandbox = sinon.createSandbox();

describe('Match model', () => {
  let playerA;
  let playerB;
  beforeEach(() => {
    playerA = {
      player: new User({
        firstName: 'fooA',
        lastName: 'barA',
        email: 'fooA@barA',
        password: 'secret',
        mmr: 1200,
        joinDate: Date.now(),
      }),
      detachments: [
        { keyword: 'Asuryani', withWarlord: true },
      ],
    };
    playerA.player.save = () => {};
    playerB = {
      player: new User({
        firstName: 'fooB',
        lastName: 'barB',
        email: 'fooB@barB',
        password: 'secret',
        mmr: 1300,
        joinDate: Date.now(),
      }),
      detachments: [
        { keyword: 'Blood Angels', withWarlord: true },
      ],
    };
    playerB.player.save = () => {};
  });
  context('check properties', () => {
    it('should have required properties', (done) => {
      const match = new Match();
      match.validate((err) => {
        expect(err.errors.playerA).to.not.exist;
        expect(err.errors.playerB).to.not.exist;
        expect(err.errors.table).to.exist;
        expect(err.errors.finished).to.not.exist;
        expect(err.errors.tournament).to.exist;
        done();
      });
    });
    it('should have optional properties', () => {
      const match = new Match({
        playerA,
        playerB,
        table: 1,
      });
      expect(match.playerA).to.have.property('score').to.be.equal(0);
      expect(match.playerB).to.have.property('score').to.be.equal(0);
      expect(match).to.have.property('table').to.be.equal(1);
      expect(match).to.have.property('finished').to.be.false;
    });
  });
  context('method finishGame', () => {
    let match;
    let saveStub;
    beforeEach(() => {
      match = new Match({
        playerA,
        playerB,
        table: 1,
      });
      saveStub = sandbox.stub(Match.prototype, 'save').yields(null, match);
    });
    afterEach(() => {
      sandbox.verifyAndRestore();
    });
    it('should set scores and finish game', (done) => {
      match.finishGame(15, 5, {}, (error, result) => {
        expect(result.playerA.score).to.be.equal(15);
        expect(result.playerB.score).to.be.equal(5);
        expect(result.finished).to.be.true;
        done();
      });
    });
    it('should call save', (done) => {
      match.finishGame(15, 5, {}, () => {
        expect(saveStub).to.have.been.calledOnce;
        done();
      });
    });
    it('should yield error', (done) => {
      sandbox.restore();
      saveStub = sandbox.stub(Match.prototype, 'save').yields(new Error('fake error'), null);
      match.finishGame(15, 5, {}, (err, result) => {
        expect(result).to.not.exist;
        expect(err.message).to.be.equal('fake error');
        done();
      });
    });
  });
});
