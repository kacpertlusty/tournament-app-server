const mongoose = require('mongoose')

const Detachment = mongoose.Schema({
  keyword: { type: String, require: true, default: 'undefined' },
  withWarlord: { type: Boolean, require: true, default: false }
})

module.exports = Detachment
