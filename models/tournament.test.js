/* global context, it, describe, */
const chai = require('chai');

const { expect } = chai;

const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

// const mongoose = require('mongoose');
// const rewire = require('rewire');

const Tournament = require('./tournament');
const User = require('./user');
const Match = require('./match');

const sandbox = sinon.createSandbox();

describe('Tournament model', () => {
  let playerA;
  let playerB;
  let saveStub;
  let tournament;
  let playerC;
  let playerD;
  let firstRound;

  beforeEach(() => {
    playerA = new User({
      firstName: 'kacper',
      lastName: 'tlusty',
      joinDate: Date.now(),
      email: 'kacper@tlusty',
      tournaments: [],
      password: 'fake pass',
    });
    playerB = new User({
      email: 'foo@bar',
      tournaments: [],
      firstName: 'foo',
      lastName: 'bar',
      joinDate: Date.now(),
      password: 'fake foo pass',
    });
    saveStub = sandbox.stub(Tournament.prototype, 'save');

    tournament = new Tournament({
      name: 'foo',
      city: 'Szczecin',
      zipCode: '72-005',
      street: 'Bar',
      creator: 'foo@bar',
      maxPlayers: 40,
      date: Date.now(),
      description: 'foo bar',
      rules: 'a',
      players: [],
      maxRounds: 5,
      finished: false,
    });
    playerC = new User({
      firstName: 'kacper',
      lastName: 'tlusty',
      joinDate: Date.now(),
      email: 'kacper@tlusty',
      tournaments: [],
      password: 'fake pass',
    });
    playerD = new User({
      email: 'foo@bar',
      tournaments: [],
      firstName: 'foo',
      lastName: 'bar',
      joinDate: Date.now(),
      password: 'fake foo pass',
    });
  });

  afterEach(() => {
    sandbox.restore();
  });

  context('check properties', () => {
    it('should have required properties', (done) => {
      const tourney = new Tournament();
      tourney.validate((err) => {
        expect(err.errors.name).to.exist;
        expect(err.errors.city).to.exist;
        expect(err.errors.zipCode).to.exist;
        expect(err.errors.street).to.exist;
        expect(err.errors.creator).to.exist;
        expect(err.errors.maxPlayers).to.exist;
        expect(err.errors.date).to.exist;
        expect(err.errors.maxRounds).to.exist;
        expect(err.errors.rank).to.not.exist;
        expect(err.errors.description).to.not.exist;
        expect(err.errors.rules).to.not.exist;
        expect(err.errors.players).to.not.exist;
        expect(err.errors.rounds).to.not.exist;
        expect(err.errors.finished).to.not.exist;
        expect(err.errors.rank).to.not.exist;
        expect(err.errors.accepted).to.not.exist;
        done();
      });
    });
    it('should have optional properties', (done) => {
      expect(tournament).to.have.property('description').to.be.equal('foo bar');
      expect(tournament).to.have.property('rules').to.be.equal('a');
      expect(tournament).to.have.property('players').to.deep.equal([]);
      expect(tournament).to.have.property('rounds').to.deep.equal([]);
      expect(tournament).to.have.property('finished').to.be.false;
      expect(tournament).to.have.property('rank').to.be.equal('Lokalny');
      expect(tournament).to.have.property('accepted').to.be.false;
      done();
    });
  });

  context('method registerUser', () => {
    const emptyArmyEntry = { detachments: [], roster: '' };
    it('should call this.save and add user to list', (done) => {
      sandbox.restore();
      saveStub = sandbox.stub(Tournament.prototype, 'save').resolves(null);
      sandbox.stub(User.prototype, 'save').resolves(null);
      tournament.registerUser(playerA, emptyArmyEntry, (err) => {
        expect(err).to.be.null;
        expect(tournament.players.length).to.be.equal(1);
        expect(tournament.players[0].player.email).to.be.equal('kacper@tlusty');
        expect(saveStub).to.be.calledOnce;
        tournament.registerUser(playerB, emptyArmyEntry, (errB) => {
          expect(errB).to.be.null;
          expect(tournament.players.length).to.be.equal(2);
          expect(saveStub).to.be.calledTwice;
          done();
        });
      });
    });
    it('should return err when same user is added', (done) => {
      sandbox.restore();
      saveStub = sandbox.stub(Tournament.prototype, 'save').resolves(null);
      sandbox.stub(User.prototype, 'save').resolves(null);
      tournament.registerUser(playerA, emptyArmyEntry, (err) => {
        expect(tournament.players.length).to.be.equal(1);
        expect(tournament.players[0].player.email).to.be.equal('kacper@tlusty');
        expect(err).to.be.null;
        expect(saveStub).to.be.calledOnce;
        tournament.registerUser(playerA, emptyArmyEntry, (errB) => {
          expect(errB.message).to.be.equal('User already registered');
          expect(tournament.players.length).to.be.equal(1);
          expect(saveStub).to.be.calledOnce;
          done();
        });
      });
    });
  });

  context('method createRound', () => {
    beforeEach(() => {
      firstRound = {
        matches: [new Match({
          playerA: {
            player: playerA,
            score: 20,
          },
          playerB: {
            player: playerB,
            score: 0,
          },
          table: 1,
        }), new Match({
          playerA: {
            player: playerC,
            score: 15,
          },
          playerB: {
            player: playerD,
            score: 5,
          },
          table: 2,
        })],
        turn: 1,
      };
      tournament.players = [
        { player: playerA, score: 20 },
        { player: playerB, score: 0 },
        { player: playerC, score: 15 },
        { player: playerD, score: 5 },
      ];
      tournament.rounds.push(firstRound);
      sandbox.restore();
    });

    it('should cb error if save rejects', (done) => {
      saveStub = sandbox.stub(Tournament.prototype, 'save').rejects(new Error('fake err'));
      tournament.save = saveStub;
      tournament.createRound((err, result) => {
        expect(err).to.not.be.null;
        expect(err.message).to.be.equal('fake err');
        expect(result).to.be.null;
        done();
      });
    });
  });

  context('method finishRound', () => {
    beforeEach(() => {
      firstRound = {
        matches: [new Match({
          playerA: {
            player: playerA,
            score: 0,
          },
          playerB: {
            player: playerB,
            score: 0,
          },
          table: 1,
          finishGame: function finishGame(a, b) {
            this.playerA.score = a;
            this.playerB.score = b;
          },
          finished: false,
          tournament: tournament._id,
        }), new Match({
          playerA: {
            player: playerC,
            score: 15,
          },
          playerB: {
            player: playerD,
            score: 5,
          },
          table: 2,
          finished: true,
          tournament: tournament._id,
        })],
        turn: 1,
      };
      tournament.players = [
        { player: playerA, score: 0 },
        { player: playerB, score: 0 },
        { player: playerC, score: 0 },
        { player: playerD, score: 0 },
      ];
      sandbox.restore();
    });
    it('should set finish property to true, and set all unfinished results to 0', (done) => {
      saveStub = sandbox.stub(Tournament.prototype, 'save').resolves(tournament);
      tournament.save = saveStub;
      tournament.finishRound(firstRound, (err, tourney) => {
        expect(err).to.be.null;
        expect(saveStub).to.have.been.calledOnce;
        expect(tourney).to.not.be.null;
        expect(firstRound.matches[0].finished).to.be.true;
        expect(firstRound.matches[0].playerA.score).to.be.equal(0);
        expect(firstRound.matches[0].playerB.score).to.be.equal(0);
        expect(firstRound.matches[1].finished).to.be.true;
        expect(firstRound.matches[1].playerA.score).to.be.equal(15);
        expect(firstRound.matches[1].playerB.score).to.be.equal(5);
        expect(tournament.players[2].score).to.be.equal(15);
        expect(tournament.players[3].score).to.be.equal(5);
        done();
      });
    });
    it('should return err when save fails', (done) => {
      saveStub = sandbox.stub(Tournament.prototype, 'save').rejects(new Error('fake err'));
      tournament.save = saveStub;
      tournament.finishRound(firstRound, (err, tourney) => {
        expect(err.message).to.be.equal('fake err');
        expect(tourney).to.be.equal(null);
        done();
      });
    });
  });
  context('method createFirstRound', () => {
    beforeEach(() => {
      playerA.mmr = 1400;
      playerB.mmr = 1000;
      playerC.mmr = 1200;
      playerD.mmr = 1200;
      tournament.players = [
        { player: playerA, score: 0 },
        { player: playerB, score: 0 },
        { player: playerC, score: 0 },
        { player: playerD, score: 0 },
      ];
      sandbox.restore();
    });
    it('should return err when save fails', (done) => {
      saveStub = sandbox.stub(Tournament.prototype, 'save').rejects(new Error('fake err'));
      tournament.createFirstRound((err, tourney) => {
        expect(err).to.exist;
        expect(tourney).to.not.exist;
        expect(err.message).to.be.equal('fake err');
        done();
      });
    });
    it('should create new round with odd number of players', (done) => {
      saveStub = sandbox.stub(Tournament.prototype, 'save').resolves(tournament);
      tournament.players.pop();
      tournament.createFirstRound((err, tourney) => {
        expect(err).to.not.exist;
        expect(tourney).to.have.property('rounds').length(1);
        expect(tourney.rounds[0].matches[0].playerA.player.id).to.be.equal(playerA.id);
        expect(tourney.rounds[0].matches[0].playerB.player.id).to.be.equal(playerC.id);
        expect(tourney.rounds[0].matches[1].playerA.player.id).to.be.equal(playerB.id);
        expect(tourney.rounds[0].matches[1].playerB.player).to.not.exist;
        expect(tourney.rounds[0].matches[1].playerA.score).to.be.equal(13);
        expect(tourney.rounds[0].matches[1].finished).to.be.true;
        done();
      });
    });
  });
  context('method finish', () => {
    beforeEach(() => {
      tournament.players = [
        { player: playerA, score: 30 },
        { player: playerB, score: 20 },
        { player: playerC, score: 50 },
        { player: playerD, score: 70 },
      ];
    });
    it('should set property finish to true and yield results', (done) => {
      sandbox.restore();
      saveStub = sandbox.stub(Tournament.prototype, 'save').yields(null, tournament);
      tournament.finish((err, results) => {
        expect(err).to.not.exist;
        expect(results[0].player.id).to.be.equal(playerD.id);
        expect(results[1].player.id).to.be.equal(playerC.id);
        expect(results[2].player.id).to.be.equal(playerA.id);
        expect(results[3].player.id).to.be.equal(playerB.id);
        expect(tournament.finished).to.be.true;
        expect(saveStub).to.have.been.calledOnce;
        done();
      });
    });
    it('should yield error if save yields error', (done) => {
      sandbox.restore();
      saveStub = sandbox.stub(Tournament.prototype, 'save').yields(new Error('fake user save error'), null);
      tournament.finish((err, result) => {
        expect(result).to.not.exist;
        expect(err.message).to.be.equal('fake user save error');
        done();
      });
    });
  });
  context('method unregister', () => {
    it('should return message that user is not registered', (done) => {
      tournament.unregister(playerA, (err, result) => {
        expect(err).to.not.exist;
        expect(result).to.be.deep.equal({ message: 'user is not registered' });
        done();
      });
    });
    it('should return error that user cannot be null', (done) => {
      tournament.unregister(null, (err, result) => {
        expect(err.message).to.be.equal('Uzytkownik musi istnieć.');
        expect(err).to.be.instanceOf(Error);
        expect(result).to.not.exist;
        done();
      });
    });
    it('should remove user from players and yield message', (done) => {
      sandbox.restore();
      saveStub = sandbox.stub(Tournament.prototype, 'save').yields(null, tournament);
      playerA.removeTournament = sandbox.stub();
      tournament.players = [
        { player: playerA, score: 0 },
        { player: playerB, score: 0 },
      ];
      tournament.unregister(playerA, (err, result) => {
        expect(err).to.not.exist;
        expect(result).to.be.deep.equal({ message: 'Zostałeś wyrejestrowany z turnieju.' });
        expect(playerA.removeTournament).to.have.been.calledOnce;
        expect(saveStub).to.have.been.calledOnce;
        done();
      });
    });
    it('should yield error when user save yields error', (done) => {
      sandbox.restore();
      saveStub = sandbox.stub(Tournament.prototype, 'save').yields(new Error('fake error'), null);
      playerA.removeTournament = sandbox.stub();
      tournament.players = [
        { player: playerA, score: 0 },
        { player: playerB, score: 0 },
      ];
      tournament.unregister(playerA, (err, result) => {
        expect(result).to.not.exist;
        expect(err.message).to.be.equal('fake error');
        expect(playerA.removeTournament).to.have.been.calledOnce;
        expect(saveStub).to.have.been.calledOnce;
        done();
      });
    });
  });
  context('method addScore', () => {
    beforeEach(() => {
      tournament.players = [
        { player: playerA, score: 10, bonusScore: 0 },
        { player: playerB, score: 0, bonusScore: 0 },
        { player: playerC, score: 0, bonusScore: 0 },
        { player: playerD, score: 0, bonusScore: 0 },
      ];
      sandbox.restore();
    });
    it('should yield error when null args are passed', (done) => {
      tournament.addScore(null, null, (err, result) => {
        expect(err.message).to.be.equal('Nie podano wymaganych argumentów');
        expect(err).to.be.instanceOf(Error);
        expect(result).to.be.null;
        done();
      });
    });
    it('should check if player is in players', (done) => {
      tournament.players.pop();
      tournament.addScore(playerD.id, 5, (err, result) => {
        expect(err).to.not.exist;
        expect(result).to.be.deep.equal({ message: 'Gracz nie bierze udziału w turnieju' });
        done();
      });
    });
    it('should check if tournament have been already finished', (done) => {
      tournament.finished = true;
      tournament.addScore(playerD.id, 5, (err, result) => {
        expect(err).to.not.exist;
        expect(result).to.be.deep.equal({ message: 'Gra juz została zakończona' });
        done();
      });
    });
    it('should add to user score, call save and return message, id, score', (done) => {
      tournament.save = sandbox.stub().yields(null, tournament);
      tournament.addScore(playerA.id, 5, (err, result) => {
        expect(err).to.not.exist;
        expect(result.name).to.be.deep.equal(tournament.name);
        expect(tournament.save).to.have.been.calledOnce;
        done();
      });
    });
    it('should yield error when save yields error', (done) => {
      tournament.save = sandbox.stub().yields(new Error('fake error'), null);
      tournament.addScore(playerA.id, 5, (err, result) => {
        expect(result).to.not.exist;
        expect(err.message).to.be.deep.equal('fake error');
        expect(err).to.be.instanceOf(Error);
        expect(tournament.save).to.have.been.calledOnce;
        done();
      });
    });
  });
});
