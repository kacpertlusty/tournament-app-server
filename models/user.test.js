/* global context, it, describe, */
const chai = require('chai');
const crypto = require('crypto');
const mongoose = require('mongoose');

const { expect } = chai;

const User = require('./user');

describe('User model', () => {
  context('check properties', () => {
    it('should have required properties', (done) => {
      const user = new User();
      user.validate((err) => {
        expect(err.errors.email).to.exist;
        expect(err.errors.firstName).to.exist;
        expect(err.errors.lastName).to.exist;
        expect(err.errors.password).to.exist;
        expect(err.errors.joinDate).to.exist;
        expect(err.errors.games).to.not.exist;
        done();
      });
    });
    it('should have optional properties', (done) => {
      const user = new User({
        firstName: 'foo',
        lastName: 'bar',
        email: 'foo@bar',
        password: 'secret_hash',
        joinDate: Date.now(),
      });
      expect(user).to.have.property('games').to.be.deep.equal([]);
      expect(user).to.have.property('tournaments').to.be.deep.equal([]);
      expect(user).to.have.property('salt').to.be.undefined;
      expect(user.mmr).to.be.equal(1500);
      done();
    });
  });
  context('method setPassword', () => {
    it('should crypt password and set salt', () => {
      const user = new User({
        firstName: 'foo',
        lastName: 'bar',
        email: 'foo@bar',
        password: 'secret_hash',
        joinDate: Date.now(),
      });
      user.setPassword('secret_hash');
      expect(user.salt).to.not.be.undefined;
      expect(user.password).to.not.be.undefined;
      expect(user.password).to.be.not.equal('secret_hash');
    });
  });
  context('method validatePassword', () => {
    let user;
    let salt;
    beforeEach(() => {
      user = new User({
        firstName: 'foo',
        lastName: 'bar',
        email: 'foo@bar',
        password: 'secret_hash',
        joinDate: Date.now(),
      });
      salt = crypto.randomBytes(16).toString('hex');
      user.password = crypto.pbkdf2Sync('secret_hash', salt, 10000, 512, 'sha512').toString('hex');
      user.salt = salt;
    });
    it('should return true if password is correct', () => {
      expect(user.validatePassword('secret_hash')).to.be.true;
    });
    it('should return false if password is incorrect', () => {
      expect(user.validatePassword('bad_pass')).to.be.false;
    });
  });
  context('method toAuthJSON', () => {
    it('should return public JSON with token', () => {
      const user = new User({
        firstName: 'foo',
        lastName: 'bar',
        email: 'foo@bar',
        password: 'secret_hash',
        joinDate: Date.now(),
      });
      user.generateJWT = () => 'token 123';
      expect(user.toAuthJSON()).to.be.deep.equal({
        _id: user._id,
        email: 'foo@bar',
        token: 'token 123',
        firstName: 'foo',
        lastName: 'bar',
      });
    });
  });
  context('method generateJWT', () => {
    it('should return jwt token', () => {
      const user = new User({
        firstName: 'foo',
        lastName: 'bar',
        email: 'foo@bar',
        password: 'secret_hash',
        joinDate: Date.now(),
      });
      const res = user.generateJWT();
      expect(res).to.exist;
      expect(res.length).to.be.greaterThan(50);
    });
  });
  context('method publicString', () => {
    it('should return basic user info', () => {
      const user = new User({
        firstName: 'foo',
        lastName: 'bar',
        email: 'foo@bar',
        password: 'secret_hash',
        joinDate: Date.now(),
      });
      const publicUser = user.publicString();
      expect(publicUser._id).to.be.deep.equal(user._id);
      expect(publicUser.firstName).to.be.equal('foo');
      expect(publicUser.lastName).to.be.equal('bar');
      expect(publicUser.games).to.be.deep.equal([]);
    });
  });
  context('method removeTournament', () => {
    let user;
    beforeEach(() => {
      user = new User({
        firstName: 'foo',
        lastName: 'bar',
        email: 'foo@bar',
        password: 'secret_hash',
        joinDate: Date.now(),
      });
    });
    it('should remove user from tournaments', () => {
      let called = false;
      user.save = () => { called = true; };
      user.tournaments.push(
        mongoose.Types.ObjectId('123123123123'),
        mongoose.Types.ObjectId('123123123124'),
      );
      user.removeTournament('123123123123');
      expect(user.tournaments.length).to.be.equal(1);
      expect(called).to.be.true;
    });
    it('should remove user from tournaments', () => {
      let called = false;
      user.save = () => { called = true; };
      user.tournaments.push(
        mongoose.Types.ObjectId('123123123123'),
        mongoose.Types.ObjectId('123123123124'),
      );
      user.removeTournament('123123123125');
      expect(user.tournaments.length).to.be.equal(2);
      expect(called).to.be.false;
    });
  });
});
