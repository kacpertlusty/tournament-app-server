const mongoose = require('mongoose')
const ranker = require('../utils/elo-ranker')
const Detachment = require('./detachment')

const MatchPlayer = mongoose.Schema({
  player: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  score: { type: Number, default: 0 },
  detachments: [Detachment]
})

const MatchSchema = mongoose.Schema({
  playerA: { type: MatchPlayer },
  playerB: { type: MatchPlayer },
  table: { type: Number, required: true },
  finished: { type: Boolean, default: false },
  tournament: { type: mongoose.Schema.Types.ObjectId, ref: 'Tournament', required: true }
})

MatchSchema.methods.finishGame = function setScore (a, b, tournament, cb) {
  if (this.finished) {
    tournament.updateScore(this.playerA.player.id, a - this.playerA.score, () => {})
    tournament.updateScore(this.playerB.player.id, b - this.playerB.score, () => {})
  }
  this.playerA.score = Number(a)
  this.finished = true
  if (this.playerB.player) {
    const { A, B } = ranker.calculate({
      A: this.playerA.player.mmr,
      B: this.playerB.player.mmr,
      score: a
    })
    this.playerB.score = Number(b)
    this.playerB.player.mmr = B
    this.playerB.player.save()
    this.playerA.player.mmr = A
    this.playerA.player.save()
  }
  this.save(cb)
}

module.exports = mongoose.model('Match', MatchSchema)
