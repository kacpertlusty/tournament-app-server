const chai = require('chai')

const { expect } = chai

const sinonChai = require('sinon-chai')

chai.use(sinonChai)

const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)

const ranker = require('./elo-ranker')

describe('elo-ranker tests', () => {
  context('calculate new elo after game finish', () => {
    it('should return null if param is missing', () => {
      expect(ranker.calculate({})).to.not.exist
      expect(ranker.calculate({ A: 1200, B: 10 })).to.not.exist
      expect(ranker.calculate({ B: 1200, difference: 10 })).to.not.exist
      expect(ranker.calculate({ A: 1200, difference: 10 })).to.not.exist
    })
    it('should return new ranking for players', () => {
      const score = ranker.calculate({ A: 1500, B: 1400, score: 13 })
      expect(score).to.be.deep.equal({ A: 1503, B: 1397 })
    })
    it('should not allow ELO lower than 800', () => {
      const score = ranker.calculate({ A: 800, B: 1400, score: 0 })
      expect(score.A).to.be.equal(800)
    })
  })
})
