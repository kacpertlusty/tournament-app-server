const config = require('../config/config')

const calculateFactorE = function calculateEa ({ A, B }) {
  if (A < 800 || B < 800) return null
  return 1 / (1 + (10 ** ((B - A) / 400)))
}

const calculate = function ranker ({
  A,
  B,
  score
}) {
  if (!A || (!score && score !== 0) || !B) return null
  const ea = calculateFactorE({ A, B }) || 0
  const eb = calculateFactorE({ A: B, B: A }) || 0
  const newA = Math.round(A + config.factorK * (score - (ea * 20)))
  const newB = Math.round(B + config.factorK * (20 - score - (eb * 20)))
  return {
    A: (newA < 800 ? 800 : newA),
    B: (newB < 800 ? 800 : newB)
  }
}

module.exports = { calculate, calculateFactorE }
