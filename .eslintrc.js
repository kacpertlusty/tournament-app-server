module.exports = {
    "extends": "standard",
    "plugins": [
        "mocha",
        "chai-friendly"
    ],
    "rules": {
        "mocha/no-exclusive-tests": "error",
        "no-unused-expressions": 0,
        "chai-friendly/no-unused-expressions": 2,
        "no-underscore-dangle": ["error", { "allow": ["_id", "__set__"] }]
    },
    "env": {
        "mocha": true,
        "node": true,
        "es6": true,
        "mongo": true
    }
};