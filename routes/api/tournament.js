const express = require('express')

const passport = require('../../config/passport')
const utils = require('../../lib/utils')
const tournamentController = require('../../controllers/tournament')

const router = express.Router()

const checkScore = (req, res, next) => {
  const { score } = req.body
  if (!score) return utils.handleError(res, new Error('missing body.score'))
  return next()
}

router.get('/',
  passport.checkToken,
  tournamentController.getWithParams)

router.post('/create',
  passport.checkToken,
  tournamentController.create)

router.put('/update/:id',
  passport.checkToken,
  tournamentController.findOne,
  tournamentController.update)

router.delete('/delete/:id',
  passport.checkToken,
  tournamentController.findOne,
  tournamentController.delete)

router.post('/:id/first-round',
  passport.checkToken,
  tournamentController.findOne,
  tournamentController.startFirstRound)

router.post('/:id/finish-round/:round',
  passport.checkToken,
  tournamentController.findOne,
  tournamentController.finishRound)

router.post('/:id/next-round',
  passport.checkToken,
  tournamentController.findOne,
  tournamentController.nextRound)

router.post('/:id/register',
  passport.checkToken,
  tournamentController.findOne,
  tournamentController.register)

router.post('/:id/finish',
  passport.checkToken,
  tournamentController.findOne,
  tournamentController.finishTournament)

router.post('/:id/unregister',
  passport.checkToken,
  tournamentController.findOne,
  tournamentController.unregisterPlayer)

router.get('/:id/results',
  passport.checkToken,
  tournamentController.findOne,
  tournamentController.getResults)

router.post('/:id/add-score/:playerId',
  passport.checkToken,
  checkScore,
  tournamentController.findOne,
  tournamentController.addScoreToPlayer)

module.exports = router
