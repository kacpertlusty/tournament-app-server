const chai = require('chai')
const sinon = require('sinon')
const sinonChai = require('sinon-chai')
const chaiAsPromised = require('chai-as-promised')
const chaiHttp = require('chai-http')
const passport = require('../../config/passport')
const app = require('../../app')
const Match = require('../../models/match')
const User = require('../../models/user')

require('sinon-mongoose')

chai.use(sinonChai)
chai.use(chaiAsPromised)
chai.use(chaiHttp)

const sandbox = sinon.createSandbox()
const { expect } = chai

describe('/api/match router', () => {
  let fakeUser
  beforeEach(() => {
    sandbox.restore()
    sinon.restore()
    fakeUser = new User({
      firstName: 'kacper',
      lastName: 'tlusty',
      email: 'foo@bar',
      password: 'secret pass',
      joinDate: Date.now()
    })
    sandbox.stub(passport, 'authenticate').callsFake((_strategy, _options, callback) => {
      callback(null, fakeUser, null)
      // eslint-disable-next-line no-unused-vars
      return (_req, res, next) => {}
    })
  })
  afterEach(() => {
    sandbox.verifyAndRestore()
    sinon.verifyAndRestore()
  })
  context('GET /api/match', () => {
    it('should return 400 if find fails', (done) => {
      sinon.mock(Match)
        .expects('find')
        .chain('populate')
        .chain('exec')
        .yields(new Error('fake error'), null)
      chai.request(app)
        .get('/api/match')
        .end((err, res) => {
          if (err) throw err
          expect(res).to.have.status(400)
          expect(res.body).to.be.deep.equal({ error: 'fake error' })
          done()
        })
    })
    it('should return 200 and list of all active matches', (done) => {
      sinon.mock(Match)
        .expects('find')
        .withArgs(sinon.match.any, 'playerA playerB table finished tournament')
        .chain('populate')
        .withArgs([
          { path: 'playerA.player', select: '_id firstName lastName mmr' },
          { path: 'playerB.player', select: '_id firstName lastName mmr' },
          { path: 'tournament', select: '_id name' }
        ])
        .chain('exec')
        .yields(null, { name: 'fake response' })
      chai.request(app)
        .get('/api/match')
        .end((err, res) => {
          if (err) throw err
          expect(res).to.have.status(200)
          expect(res.body.match).to.be.deep.equal({ name: 'fake response' })
          done()
        })
    })
    it('should return 200 and use player in query', (done) => {
      sinon.mock(Match)
        .expects('find')
        .withArgs({ $and: [{ $or: [{ 'playerA.player': '1' }, { 'playerB.player': '1' }] }] }, 'playerA playerB table finished tournament')
        .chain('populate')
        .withArgs([
          { path: 'playerA.player', select: '_id firstName lastName mmr' },
          { path: 'playerB.player', select: '_id firstName lastName mmr' },
          { path: 'tournament', select: '_id name' }
        ])
        .chain('exec')
        .yields(null, { name: 'fake response' })
      chai.request(app)
        .get('/api/match?player=1')
        .end((err, res) => {
          if (err) throw err
          expect(res).to.have.status(200)
          expect(res.body.match).to.be.deep.equal({ name: 'fake response' })
          done()
        })
    })
    it('should return 401 if authorization is missing', (done) => {
      sandbox.restore()
      chai.request(app)
        .get('/api/match')
        .end((err, res) => {
          if (err) throw err
          expect(res).to.have.status(401)
          expect(res.body).to.be.deep.equal({ message: 'No auth token' })
          done()
        })
    })
    it('should return 200 and use player, finished, tournament in query', (done) => {
      sinon.mock(Match)
        .expects('find')
        .withArgs({
          $and: [
            {
              $or: [
                { 'playerA.player': '1' },
                { 'playerB.player': '1' }
              ]
            },
            { tournament: '123123' },
            { finished: false }
          ]
        }, 'playerA playerB table finished tournament')
        .chain('populate')
        .withArgs([
          { path: 'playerA.player', select: '_id firstName lastName mmr' },
          { path: 'playerB.player', select: '_id firstName lastName mmr' },
          { path: 'tournament', select: '_id name' }
        ])
        .chain('exec')
        .yields(null, { name: 'fake response' })
      chai.request(app)
        .get('/api/match?player=1&tournament=123123&finished=false')
        .end((err, res) => {
          if (err) throw err
          expect(res).to.have.status(200)
          expect(res.body.match).to.be.deep.equal({ name: 'fake response' })
          done()
        })
    })
  })
  context('POST /api/match/:id/finish', () => {
    it('should return 401 if authorization fails', (done) => {
      sandbox.restore()
      chai.request(app)
        .post('/api/match/:id/finish')
        .end((err, res) => {
          if (err) throw err
          expect(res).to.have.status(401)
          expect(res.body).to.be.deep.equal({ message: 'No auth token' })
          done()
        })
    })
    it('should return 200 and reject when no results are given', (done) => {
      chai.request(app)
        .post('/api/match/123/finish')
        .end((err, res) => {
          if (err) throw err
          expect(res).to.have.status(200)
          expect(res.body).to.be.deep.equal({ message: 'no score in body' })
          done()
        })
    })
  })
})
