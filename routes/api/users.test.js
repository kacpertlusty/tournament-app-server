const chai = require('chai');
const sinonChai = require('sinon-chai');
const chaiAsPromised = require('chai-as-promised');
const mongoose = require('mongoose');
const chaiHttp = require('chai-http');
const sinon = require('sinon');
const passport = require('../../config/passport');
const User = require('../../models/user');
const app = require('../../app');

require('sinon-mongoose');


chai.use(sinonChai);
chai.use(chaiAsPromised);
chai.use(chaiHttp);
const sandbox = sinon.createSandbox();
const { expect } = chai;

describe('/api/user router', () => {
  const fakeUser = {
    id: '123',
    firstName: 'foo',
    lastName: 'bar',
    email: 'foo@bar',
    password: 'secret_hash',
    joinDate: Date(1549652632784),
    games: ['Warhammer 40000', 'Age of Sigmar'],
  };

  beforeEach(() => {
    sandbox.stub(passport, 'authenticate').callsFake((_strategy, _options, callback) => {
      callback(null, fakeUser, null);
      // eslint-disable-next-line no-unused-vars
      return (req, res, next) => {};
    });
  });

  afterEach(() => {
    sandbox.restore();
    sinon.restore();
    mongoose.connection.close();
  });

  context('POST /create', () => {
    let saveStub;
    const req = {
      firstName: 'foo1',
      lastName: 'bar1',
      email: 'foo1@bar.com',
      password: 'secret_hash',
    };
    it('should create user', (done) => {
      saveStub = sandbox.stub(User.prototype, 'save').yields(null, fakeUser);
      chai.request(app).post('/api/user/create')
        .send(req)
        .end((err, res) => {
          if (err) throw err;
          expect(saveStub).to.have.been.calledOnce;
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          done();
        });
    });
    it('should return 400 if fails', (done) => {
      saveStub = sandbox.stub(User.prototype, 'save').yields('Fake error', null);
      chai.request(app).post('/api/user/create')
        .send(req)
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(400);
          expect(res).to.be.json;
          done();
        });
    });
    it('should return 400 if body is missing', (done) => {
      chai.request(app).post('/api/user/create')
        .send()
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(400);
          expect(res).to.be.json;
          done();
        });
    });
    it('should return 400 if password is missing', (done) => {
      chai.request(app).post('/api/user/create')
        .send({ email: 'foo@bar' })
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(400);
          expect(res).to.be.json;
          done();
        });
    });
    it('should return 400 if email is missing', (done) => {
      chai.request(app).post('/api/user/create')
        .send({ password: 'secret password' })
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(400);
          expect(res).to.be.json;
          done();
        });
    });
  });

  context('GET /:id', () => {
    it('should return user', (done) => {
      sinon.mock(User)
        .expects('findById')
        .withArgs('123', '_id firstName lastName email mmr games tournaments')
        .chain('populate')
        .withArgs({
          path: 'games',
          select: '_id name playerA playerB table finished',
          populate: [
            { path: 'playerA.player', select: 'firstName lastName score id' },
            { path: 'playerB.player', select: 'firstName lastName score id' },
          ],
        })
        .chain('populate')
        .withArgs({
          path: 'tournaments',
          select: '_id name players',
          populate: [
            { path: 'players.player', select: 'firstName lastName id' },
          ],
          match: { 'players.player': '123' },
        })
        .chain('exec')
        .yields(null, fakeUser);
      chai.request(app).get('/api/user/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          done();
        });
    });
    it('should return 400 if User.findById yields error', (done) => {
      sinon.mock(User)
        .expects('findById')
        .withArgs('123', '_id firstName lastName email mmr games tournaments')
        .chain('populate')
        .withArgs({
          path: 'games',
          select: '_id name playerA playerB table finished',
          populate: [
            { path: 'playerA.player', select: 'firstName lastName score id' },
            { path: 'playerB.player', select: 'firstName lastName score id' },
          ],
        })
        .chain('populate')
        .withArgs({
          path: 'tournaments',
          select: '_id name players',
          populate: [
            { path: 'players.player', select: 'firstName lastName id' },
          ],
          match: { 'players.player': '123' },
        })
        .chain('exec')
        .yields(new Error('fake error'), null);
      chai.request(app).get('/api/user/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(400);
          expect(res).to.be.json;
          done();
        });
    });
    it('should return 401 if authorization fails', () => {
      sandbox.restore();
      chai.request(app).get('/api/user/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(401);
          expect(res).to.be.json;
        });
    });
  });

  context('UPDATE /update/:id', () => {
    const req = {
      firstName: 'new first name',
      lastName: 'new last name',
      email: 'new email',
    };
    it('should return 401 if authorization fails', (done) => {
      sandbox.restore();
      chai.request(app)
        .put('/api/user/update/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(401);
          expect(res.text).to.be.deep.equal('{"message":"No auth token"}');
          done();
        });
    });
    it('should return 400 if trying to change other user\'s id', (done) => {
      fakeUser._id = '123123123123';
      chai.request(app)
        .put('/api/user/update/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(400);
          expect(res.body).to.be.deep.equal({ error: 'Missing credentials' });
          done();
        });
    });
    it('should return 200 update user', (done) => {
      sinon.mock(User)
        .expects('findById')
        .withArgs('123')
        .yields(null, fakeUser);
      fakeUser._id = '123';
      fakeUser.save = cb => cb(null, fakeUser);
      fakeUser.publicString = () => 'fake public';
      chai.request(app)
        .put('/api/user/update/123')
        .send(req)
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(200);
          expect(res.body).to.be.equal('fake public');
          done();
        });
    });
    it('should return 400 error if user findById yields error', (done) => {
      sinon.mock(User)
        .expects('findById')
        .withArgs('123')
        .yields(new Error('fake error'), null);
      chai.request(app)
        .put('/api/user/update/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(400);
          expect(res.body).to.be.deep.equal({ error: 'fake error' });
          done();
        });
    });
    it('should return 400 error if user save yields error', (done) => {
      sinon.mock(User)
        .expects('findById')
        .withArgs('123')
        .yields(null, fakeUser);
      fakeUser._id = '123';
      fakeUser.save = cb => cb(new Error('fake save error'), null);
      chai.request(app)
        .put('/api/user/update/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(400);
          expect(res.body).to.be.deep.equal({ error: 'fake save error' });
          done();
        });
    });
  });
  context('DELETE /delete/:id', () => {
    it('should return 401 if authorization fails', (done) => {
      sandbox.restore();
      chai.request(app)
        .delete('/api/user/delete/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(401);
          expect(res.body).to.be.deep.equal({ message: 'No auth token' });
          done();
        });
    });
    it('should return 200 and delete user', (done) => {
      sinon.mock(User)
        .expects('findByIdAndDelete')
        .withArgs('123')
        .yields(null, fakeUser);
      chai.request(app)
        .delete('/api/user/delete/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(200);
          expect(res.body).to.be.deep.equal({ userId: '123', message: 'User deleted' });
          done();
        });
    });
    it('should return 401 if id\'s doesn\'t match', (done) => {
      fakeUser._id = 'asdfas23141234';
      chai.request(app)
        .delete('/api/user/delete/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(401);
          expect(res.body).to.be.deep.equal({ error: 'Missing credentials' });
          done();
        });
    });
    it('should return 400 if User findByIdAndDelete yields error', (done) => {
      sinon.mock(User)
        .expects('findByIdAndDelete')
        .withArgs('123')
        .yields(new Error('fake delete error'), null);
      fakeUser._id = '123';
      chai.request(app)
        .delete('/api/user/delete/123')
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(400);
          expect(res.body).to.be.deep.equal({ error: 'fake delete error' });
          done();
        });
    });
  });
  context('POST /login', () => {
    it('should authorize correctly', (done) => {
      sandbox.stub(passport, 'authorize').callsFake((_strategy, callback) => {
        callback(null, fakeUser, null);
        // eslint-disable-next-line no-unused-vars
        return (req, res, next) => {};
      });
      fakeUser.toAuthJSON = () => 'some token';
      chai.request(app)
        .post('/api/user/login')
        .auth('foo@bar', 'secret pass', { type: 'basic' })
        .end((err, res) => {
          if (err) throw err;
          expect(res).to.have.status(200);
          expect(res.body).to.be.deep.equal({ token: 'some token' });
          done();
        });
    });
  });
  it('should return 401 if authorization yields error', (done) => {
    sandbox.restore();
    sandbox.stub(passport, 'authorize').callsFake((_strategy, callback) => {
      callback({ error: 'wrong auth' }, null, null);
      // eslint-disable-next-line no-unused-vars
      return (req, res, next) => {};
    });
    chai.request(app)
      .post('/api/user/login')
      .auth('no email', 'bad password', { type: 'basic' })
      .end((err, res) => {
        if (err) throw err;
        expect(res).to.have.status(401);
        expect(res.body).to.be.deep.equal({ error: 'wrong auth' });
        done();
      });
  });
  it('should return 401 if user not found', (done) => {
    sandbox.stub(passport, 'authorize').callsFake((_strategy, callback) => {
      callback(null, null, 'fake info');
      // eslint-disable-next-line no-unused-vars
      return (req, res, next) => {};
    });
    chai.request(app)
      .post('/api/user/login')
      .auth('no email', 'bad password', { type: 'basic' })
      .end((err, res) => {
        if (err) throw err;
        expect(res).to.have.status(401);
        expect(res.body).to.be.deep.equal('fake info');
        done();
      });
  });
});
