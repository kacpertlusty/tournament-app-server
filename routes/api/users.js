const express = require('express');
const User = require('../../models/user');
const passport = require('../../config/passport');
const utils = require('../../lib/utils');

const router = express.Router();

router.get('/:id', passport.checkToken, (req, res) => User.findById(req.params.id, '_id firstName lastName email mmr games tournaments')
  .populate({
    path: 'games',
    select: '_id name playerA playerB table finished',
    populate: [
      { path: 'playerA.player', select: 'firstName lastName score id' },
      { path: 'playerB.player', select: 'firstName lastName score id' },
    ],
  })
  .populate({
    path: 'tournaments',
    select: '_id name players',
    populate: [
      { path: 'players.player', select: 'firstName lastName id' },
    ],
    match: { 'players.player': req.params.id },
  })
  .exec((err, user) => {
    if (err) return utils.handleError(res, err);
    return res.status(200).json(user);
  }));

router.post('/login', (req, res, next) => {
  passport.authorize('local', (err, user, info) => {
    if (err) {
      return res.status(401).json(err);
    }
    if (user) {
      const token = user.toAuthJSON();
      return res.status(200).json({
        token,
      });
    }
    return res.status(401).json(info);
  })(req, res, next);
});

router.post('/create', (req, res) => {
  if (!req.body || !req.body.email || !req.body.password) return utils.handleError(res, new Error('Missing arguments'));
  req.body.joinDate = Date.now();
  const user = new User(req.body);
  user.setPassword(req.body.password);
  return user.save((err, saved) => {
    if (err) return utils.handleError(res, err);
    return res.status(200).json({ message: 'User created', userId: saved.id });
  });
});

router.put('/update/:id',
  passport.checkToken,
  (req, res) => {
    if (req.params.id !== req.user._id) return utils.handleError(res, new Error('Missing credentials'));
    return User.findById(req.user.id, (err, foundUser) => {
      if (err) return utils.handleError(res, err);
      const user = foundUser;
      if (req.body.firstName) user.firstName = req.body.firstName;
      if (req.body.lastName) user.lastName = req.body.lastName;
      if (req.body.email) user.email = req.body.email;
      return user.save((error, result) => {
        if (error) return utils.handleError(res, error);
        return res.status(200).json(result.publicString());
      });
    });
  });

router.delete('/delete/:id',
  passport.checkToken,
  (req, res) => {
    if (req.params.id !== req.user._id) return utils.handleError(res, new Error('Missing credentials'), 401);
    return User.findByIdAndDelete(req.params.id, (err, result) => {
      if (err) return utils.handleError(res, err);
      return res.status(200).json({ userId: result._id, message: 'User deleted' });
    });
  });

module.exports = router;
