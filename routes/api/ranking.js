const express = require('express');
const passport = require('../../config/passport');
const User = require('../../models/user');
const utils = require('../../lib/utils');

const router = express.Router();

router.get('/', passport.checkToken, (req, res) => User
  .find(req.query)
  .select('id firstName lastName mmr')
  .sort({ mmr: -1 })
  .exec((err, users) => {
    if (err) return utils.handleError(res, err, 503);
    return res.status(200).json(users);
  }));

module.exports = router;
