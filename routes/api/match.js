const express = require('express')

const passport = require('../../config/passport')
const tournamentController = require('../../controllers/tournament')
const matchController = require('../../controllers/match')

const router = express.Router()

router.get('/',
  passport.checkToken,
  matchController.get,
  (_, res) => res.status(200).json({ match: res.locals.match }))

router.post('/:id/finish',
  passport.checkToken,
  matchController.validateScore,
  matchController.getById,
  matchController.finish,
  (_, res) => res.status(200).json({ match: res.locals.match }))

router.put('/:id/finish',
  passport.checkToken,
  matchController.validateScore,
  tournamentController.findTournamentForMatch,
  matchController.getById,
  matchController.finish,
  (req, res) => res.status(200).json({ match: res.locals.match }))

module.exports = router
