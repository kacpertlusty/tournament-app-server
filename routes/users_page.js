const mongoose = require('mongoose')
const passport = require('passport')
const express = require('express')
const auth = require('./auth')
const userController = require('../controllers/users')

const User = mongoose.model('User')

const router = express.Router()

router.get('/', (req, res) => {
  res.render('add_user')
})

router.get('/login', auth.optional, (req, res) => {
  if (!req.payload) return res.render('login')

  const { payload: { id } } = req

  return userController.get(id, (err, user) => {
    if (err) return res.render('err', err)
    if (!user) {
      return res.render('login')
    }
    return res.render('index')
  })
})

router.post('/login', passport.authenticate(['local', 'jwt'], { failureRedirect: '/login', successRedirect: '/' }))

router.get('/current', auth.required, (req, res) => {
  const { payload: { id } } = req

  return User.findById(id)
    .then((user) => {
      if (!user) {
        return res.sendStatus(400)
      }

      return res.json({ user: user.toAuthJSON() })
    })
})

module.exports = router
