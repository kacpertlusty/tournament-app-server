const mongoose = require('mongoose')
// const passport = require('passport');
const express = require('express')
const createError = require('http-errors')
const passport = require('../config/passport')
const auth = require('./auth')
// const User = mongoose.model('User');
const Tournament = mongoose.model('Tournament')
const router = express.Router()

const projection = 'name street city maxPlayers maxRounds description rules rounds players date'
router.get('/show/:id', passport.loginRequired, (req, res) => Tournament.findById(
  req.params.id
).populate('creator').exec((err, tournament) => {
  if (err) return res.render('error', { error: err })
  if (!tournament) return res.render('error', { error: new Error('No tournament') })
  const canJoin = tournament.players
  // eslint-disable-next-line no-underscore-dangle
    .filter(p => p.player.id.toString() === req.user._id).length === 0
  return res.status(200).render('tournament_details', { tournament, canJoin })
}))

router.get('/:id/add/user', passport.loginRequired, (req, res, next) => {
  const { user } = req
  try {
    Tournament.findById(req.params.id, (err, tournament) => {
      if (err) return next(createError(500, err))
      if (!tournament) return res.render('error', { error: new Error('tournament not found') })
      return tournament.registerUser(user, (error, obj) => {
        if (error) res.render('error', { error })
        res.render('tournament_details', { tournament: obj })
      })
    })
  } catch (err) {
    next(err)
  }
})

router.get('/show', (req, res) => Tournament.find({ },
  projection, (err, tournaments) => {
    if (err) return res.status(400).json({ err })
    // res.locals.tournaments = tournaments;
    return res.status(200).render('all_tournaments', { tournaments })
  }))

router.get('/edit/:id', passport.loginRequired, (req, res) => Tournament.find({ _id: req.params.id }, projection, (error, tournament) => {
  if (error) return res.status(400).render('error', { error })
  return res.status(200).render('edit_tournament', { tournament: tournament[0] })
}))

router.post('/add', passport.loginRequired, (req, res) => {
  if (!req.body) return res.render('error', { err: new Error('Invalid arguments') })
  // eslint-disable-next-line no-underscore-dangle
  req.body.creator = req.user._id
  const tournament = new Tournament(req.body)
  return tournament.save()
    .then(tourney => res.redirect(`/tournament/show/${tourney.id}`), err => res.render('error', { error: err }))
})

router.get('/add', auth.optional, (req, res) => {
  if (!req.user) return res.redirect('/users/login')
  return res.render('add_tournament')
})

router.get('/:id/create-first-round', (req, res) => {
  if (!req.user) return res.redirect('/users/login')
  return Tournament.findById(req.params.id, (err, tourney) => {
    if (err) return res.render('error', { error: err })
    return tourney.createFirstRound((error, tournament) => {
      if (error) return res.render('error', { error })
      return res.render('tournament_details', { tournament })
    })
  })
})

module.exports = router
