const chai = require('chai')

const { expect } = chai

const sinon = require('sinon')
const sinonChai = require('sinon-chai')

chai.use(sinonChai)

const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)

const mongoose = require('mongoose')
const rewire = require('rewire')
const User = require('../models/user')
const Mailer = require('../lib/mailer')

let users = rewire('./users')

const sandbox = sinon.createSandbox()

describe('users controller', () => {
  let findStub
  let sampleUserSaveStub
  let sampleArgs
  let sampleUser
  let samplePublicUser

  beforeEach(() => {
    sampleArgs = {
      firstName: 'foo',
      lastName: 'bar',
      email: 'foo@bar',
      password: 'secret_hash',
      games: ['Warhammer 40000', 'Age of Sigmar']
    }
    samplePublicUser = {
      _id: 123,
      firstName: 'foo',
      lastName: 'bar',
      email: 'foo@bar',
      games: ['Warhammer 40000', 'Age of Sigmar']
    }
    sampleUserSaveStub = sandbox.stub().resolves(sampleArgs)
    sampleUser = {
      id: 123,
      firstName: 'foo',
      lastName: 'bar',
      email: 'foo@bar',
      games: ['Warhammer 40000', 'Age of Sigmar'],
      save: sampleUserSaveStub,
      password: 'secret',
      joinDate: Date.now(),
      publicString: () => samplePublicUser
    }

    findStub = sandbox.stub(mongoose.Model, 'findById').yields(null, sampleUser)
  })

  afterEach(() => {
    sandbox.restore()
    users = rewire('./users')
  })

  context('delete', () => {
    let removeStub

    afterEach(() => {
      sandbox.restore()
      users = rewire('./users')
    })
    it('should reject if User.deleteOne', () => {
      sandbox.restore()
      removeStub = sandbox.stub(mongoose.Model, 'deleteOne').yields(new Error('fake_reject'), null)
      expect(users.delete({ id: 123 })).to.eventually.be.rejectedWith('fake_reject')
    })

    it('should check id', () => {
      expect(users.delete()).to.eventually.be.rejectedWith('Invalid object')
      expect(users.delete({})).to.eventually.be.rejectedWith('Invalid id')
    })

    it('should call User.delete', async () => {
      removeStub = sandbox.stub(mongoose.Model, 'deleteOne').yields(null)

      const args = { id: 123 }
      const result = await users.delete(args)

      expect(result).to.be.deep.equal({ message: 'Deleted user 123' })
      expect(removeStub).to.have.been.calledOnceWith(args)
    })
  })

  context('create', () => {
    let result
    let FakeMailer
    let mailerStub
    let FakeUserClass
    let saveStub
    beforeEach(async () => {
      mailerStub = sandbox.stub(Mailer.prototype, 'sendWelcomeEmail').resolves('fake_email')
      FakeMailer = sandbox.stub().returns({ sendWelcomeEmail: mailerStub })
      // eslint-disable-next-line no-underscore-dangle
      users.__set__('Mailer', FakeMailer)
      saveStub = sandbox.stub(User.prototype, 'save').resolves(sampleUser)
      FakeUserClass = sandbox.stub().returns({
        save: saveStub,
        setPassword: () => 'secret'
      })
      // eslint-disable-next-line no-underscore-dangle
      users.__set__('User', FakeUserClass)

      result = await users.create(sampleUser)
    })

    afterEach(() => {
      sandbox.restore()
      users = rewire('./users')
    })

    it('check for required parameters', async () => {
      await expect(users.create()).to.eventually.be.rejectedWith('No object given')
      await expect(users.create({
        lastName: 'bar',
        email: 'foo@bar.com',
        password: 'secret'
      })).to.eventually.be.rejectedWith('Invalid arguments')
      await expect(users.create({
        firstName: 'foo',
        email: 'foo@bar.com',
        password: 'secret'
      })).to.eventually.be.rejectedWith('Invalid arguments')
      await expect(users.create({
        firstName: 'foo',
        lastName: 'bar',
        password: 'secret'
      })).to.eventually.be.rejectedWith('Invalid arguments')
      await expect(users.create({
        firstName: 'foo',
        lastName: 'bar',
        email: 'foo@bar.com'
      })).to.eventually.be.rejectedWith('Invalid arguments')
      await expect(users.create({
        firstName: null,
        lastName: null,
        email: null
      })).to.eventually.be.rejectedWith('Invalid arguments')
    })

    it('should call User with new', () => {
      expect(FakeUserClass).to.have.been.calledWithNew
      expect(FakeUserClass).to.have.been.calledWith(sampleUser)
    })

    it('should save the user', () => {
      expect(saveStub).to.have.been.called
    })

    it('should call email with email, first name and last name', () => {
      expect(mailerStub).to.have.been
        .calledWith(sampleUser.email, sampleUser.firstName, sampleUser.lastName)
    })

    it('should reject with err', () => {
      saveStub.rejects(new Error('fake'))
      expect(users.create(sampleUser)).to.eventually.be.rejectedWith('fake')
    })

    it('should return message and id', async () => {
      expect(result).to.have.property('userId', 123)
      expect(result).to.have.property('message', 'User created')
    })
  })

  context('update', () => {
    let result
    beforeEach(async () => {
      result = await users.update(123, sampleArgs)
    })

    it('should check for data', () => {
      expect(users.update(null, null)).to.eventually.be.rejected
      expect(users.update(null, null)).to.eventually.be.rejectedWith('Invalid data')
    })

    it('should check for id', () => {
      expect(users.update(null, {})).to.eventually.be.rejected
      expect(users.update(null, {})).to.eventually.be.rejectedWith('Invalid id')
    })

    it('should return updated user', () => {
      expect(result).to.exist
      expect(result).to.have.property('firstName', 'foo')
      expect(result).to.have.property('games')
      expect(result).to.have.property('lastName', 'bar')
      expect(result).to.have.property('password')
    })

    it('should call findUserById', () => {
      expect(findStub).to.be.called
      expect(findStub).to.be.calledWith(123)
    })

    it('should call user.save', () => {
      expect(sampleUser.save).to.have.be.calledOnce
    })

    it('should return error if find user returns error', () => {
      sandbox.restore()
      findStub = sandbox.stub(mongoose.Model, 'findById').yields(new Error('fake error', null))
      expect(users.update(123, {})).to.eventually.be.rejectedWith('fake error')
    })
  })

  context('get', () => {
    it('should check for id', (done) => {
      users.get(null, (err, result) => {
        expect(err).to.exist
        expect(err.message).to.deep.equal('Invalid id')
        expect(result).to.not.exist
        done()
      })
    })

    it('should call findUserById and return result', (done) => {
      users.get(123, (err, result) => {
        expect(err).to.not.exist
        expect(result).to.exist
        // eslint-disable-next-line no-underscore-dangle
        expect(result._id).to.equal(123)
        expect(result.firstName).to.equal('foo')
        expect(result.lastName).to.equal('bar')
        expect(findStub).to.have.been.calledWith(123)
        done()
      })
    })

    it('should return error if findUserById returns Error', (done) => {
      sandbox.restore()
      findStub = sandbox.stub(mongoose.Model, 'findById').yields(new Error('fake error', null))
      users.get(123, (err, result) => {
        expect(err).to.exist
        expect(err.message).to.equal('fake error')
        expect(result).to.not.exist
        expect(findStub).to.have.been.calledWith(123)
        done()
      })
    })
  })
})
