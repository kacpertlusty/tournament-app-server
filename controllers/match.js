const Match = require('../models/match')
const utils = require('../lib/utils')

const addMatchToLocals = (res, match) => {
  if (!res.locals) res.locals = {}
  res.locals.match = match
}

const getQuery = (req) => {
  const { player, tournament, finished } = req.query
  let query = { $and: [] }
  if (player) query.$and.push({ $or: [{ 'playerA.player': player }, { 'playerB.player': player }] })
  if (tournament) query.$and.push({ tournament })
  if (finished !== undefined) query.$and.push({ finished: finished === 'true' })
  return query.$and.length ? query : {}
}

const getWithQuery = (req, res, next) => Match
  .find(getQuery(req), 'playerA playerB table finished tournament')
  .populate([
    { path: 'playerA.player', select: '_id firstName lastName mmr' },
    { path: 'playerB.player', select: '_id firstName lastName mmr' },
    { path: 'tournament', select: '_id name' }
  ]).exec((err, match) => {
    if (err) return utils.handleError(res, err)
    addMatchToLocals(res, match)
    return next()
  })

const validateScore = (req, res, next) => {
  const { score } = req.body
  if (!utils.validateScore(score.a, score.b)) {
    return utils.handleError(res, new Error('Wprowadzono błędny wynik'), 400)
  }
  return next()
}

const findMatchById = (req, res, next) => Match
  .findById(req.params.id)
  .populate([
    { path: 'playerA.player', select: '_id mmr' },
    { path: 'playerB.player', select: '_id mmr' },
    { path: 'tournament' }
  ])
  .exec((error, match) => {
    if (error) return utils.handleError(res, error, 500)
    addMatchToLocals(res, match)
    return next()
  })

const finishMatch = (req, res, next) => {
  const { match } = res.locals
  const { id } = req.user
  const { score } = req.body
  if (match.playerA.player.id !== id && match.playerB.player.id !== id) {
    return utils.handleError(res, new Error('Forbidden'), 403)
  }
  if (match.finished) return res.status(200).json({ message: 'game already finished' })
  return match.finishGame(score.a, score.b, match.tournament, (err, finishedMatch) => {
    if (err) return utils.handleError(res, err, 500)
    addMatchToLocals(res, finishMatch)
    return next()
  })
}

const updateMatch = (req, res, next) => {
  const { match, tournament } = res.locals
  const { score } = req.body
  if (!match) return utils.handleError(res, new Error('match not found'), 404)
  return match.finishGame(score.a, score.b, tournament, (error, updatedMatch) => {
    if (error) return utils.handleError(res, error, 500)
    addMatchToLocals(res, updateMatch)
    return next()
  })
}

module.exports = {
  get: getWithQuery,
  getById: findMatchById,
  finish: finishMatch,
  update: updateMatch,
  validateScore
}
