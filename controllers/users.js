// const mongoose = require('mongoose');
const User = require('../models/user')
const Mailer = require('../lib/mailer')

exports.create = function createPrivateFunc (data) {
  if (!data) return Promise.reject(new Error('No object given'))
  if (!data.firstName || !data.lastName || !data.email || !data.password) {
    return Promise.reject(new Error('Invalid arguments'))
  }
  const user = new User(data)
  user.setPassword(user.password)
  user.joinDate = Date.now()

  return user.save().then(
    saved => (new Mailer()).sendWelcomeEmail(saved.email, saved.firstName, saved.lastName).then(
      () => Promise.resolve({ message: 'User created', userId: saved.id }), emailReject => Promise.reject(emailReject)
    ), rejected => Promise.reject(rejected)
  )
}

exports.update = function updateUser (id, data) {
  if (!data) return Promise.reject(new Error('Invalid data'))
  if (!id) return Promise.reject(new Error('Invalid id'))
  return User.findById(id, (err, usr) => {
    if (err) return Promise.reject(err)
    const returnUser = usr
    Object.keys(data).forEach((key) => {
      returnUser[key] = data[key]
    })
    return returnUser.save()
      .then(result => Promise.resolve(result),
        reject => Promise.reject(reject))
  })
}

exports.get = function getUserById (userId, callback) {
  if (!userId) return callback(new Error('Invalid id'), null)
  return User.findById(userId, (err, user) => {
    if (err) return callback(err, null)
    return callback(null, user.publicString())
  })
}

exports.getWithParams = function getAllUsers (params, callback) {
  if (!params) return callback(new Error('Invalid param'), null)
  return ''
}

exports.delete = function deleteUserById (data) {
  if (!data) return Promise.reject(new Error('Invalid object'))
  if (!data.id) return Promise.reject(new Error('Invalid id'))
  return User.deleteOne(data, (err) => {
    if (err) return Promise.reject(err)
    return Promise.resolve({ message: `Deleted user ${data.id}` })
  })
}
