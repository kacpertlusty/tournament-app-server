const chai = require('chai')

const { expect } = chai

const sinon = require('sinon')
const sinonChai = require('sinon-chai')

chai.use(sinonChai)

const chaiAsPromised = require('chai-as-promised')

chai.use(chaiAsPromised)

const mongoose = require('mongoose')
const rewire = require('rewire')
const Tournament = require('../models/tournament')

let tournamentController = rewire('./tournament')

const sandbox = sinon.createSandbox()

describe('tournament controller', () => {
  let findStub
  let tournamentSaveStub
  let sampleArgs
  let sampleTournament
  let req
  let statusStub
  let jsonStub
  let res
  let handleErrorStub
  let saveStub

  const error = new Error('fake error')
  beforeEach(() => {
    sampleArgs = {
      name: 'foo',
      city: 'Szczecin',
      zipCode: '72-005',
      street: 'Bar',
      creator: {
        id: 'fake_id',
        email: 'foo@bar'
      },
      maxPlayers: 40,
      date: Date.now(),
      description: 'foo bar',
      rules: 'a',
      players: [],
      rank: 'Lokalny'
    }
    tournamentSaveStub = sandbox.stub().resolves(sampleArgs)
    sampleTournament = {
      id: 123,
      name: 'foo',
      city: 'Szczecin',
      zipCode: '72-005',
      street: 'Bar',
      creator: {
        id: 'fake_id',
        email: 'foo@bar'
      },
      maxPlayers: 40,
      date: Date.now(),
      description: 'foo bar',
      rules: 'a',
      players: [],
      save: tournamentSaveStub
    }
    jsonStub = sandbox.stub()
    statusStub = sandbox.stub().returns({ json: jsonStub })
    res = { status: statusStub }
    handleErrorStub = sandbox.spy()
    tournamentController.__set__('utils', { handleError: handleErrorStub })
    findStub = sandbox.stub(mongoose.Model, 'findById').yields(null, sampleTournament)
    saveStub = sandbox.stub(Tournament.prototype, 'save')
  })

  afterEach(() => {
    sandbox.restore()
    tournamentController = rewire('./tournament')
  })

  context('delete', () => {
    let removeStub
    beforeEach(() => {
      req = {
        locals: {
          tournament: sampleTournament
        },
        user: {
          id: 'fake_id'
        },
        params: {
          id: 123
        }
      }
    })
    it('should call utils handle error', () => {
      removeStub = sandbox.stub(mongoose.Model, 'findByIdAndDelete').yields(error, null)
      tournamentController.delete(req, res)
      expect(jsonStub.called).to.be.false
      expect(statusStub.called).to.be.false
      expect(handleErrorStub.calledOnceWith(res, error)).to.be.true
      expect(removeStub.calledOnceWith(123, sinon.match.func)).to.be.true
    })
    it('should call handle error with 401 status code if user is not tournament owner', () => {
      req.user.id = 'another_fake_id'
      removeStub = sandbox.stub(mongoose.Model, 'findByIdAndDelete')
      tournamentController.delete(req, res)
      expect(jsonStub.called).to.be.false
      expect(statusStub.called).to.be.false
      expect(handleErrorStub.calledOnceWith(res, { message: 'Missing privileges' }, 401)).to.be.true
      expect(removeStub.called).to.be.false
    })
    it('should call handle error if tournament already finished', () => {
      req.locals.tournament.finished = true
      removeStub = sandbox.stub(mongoose.Model, 'findByIdAndDelete')
      tournamentController.delete(req, res)
      expect(jsonStub.called).to.be.false
      expect(statusStub.called).to.be.false
      expect(handleErrorStub.calledOnceWith(res, { message: 'cannot delete finished tournament' })).to.be.true
      expect(removeStub.called).to.be.false
    })
    it('should return 200 and result when delete is successful', () => {
      removeStub = sandbox.stub(mongoose.Model, 'findByIdAndDelete').yields(null, 'fake_res')
      tournamentController.delete(req, res)
      expect(jsonStub.calledOnceWith('fake_res')).to.be.true
      expect(statusStub.calledOnceWith(200)).to.be.true
      expect(handleErrorStub.called).to.be.false
      expect(removeStub.calledOnceWith(123, sinon.match.func)).to.be.true
    })
  })

  context('create', () => {
    let FakeTournamentClass
    let saveObj
    beforeEach(() => {
      saveObj = { save: saveStub }
      FakeTournamentClass = sandbox.stub().returns(saveObj)
      // eslint-disable-next-line no-underscore-dangle
      tournamentController.__set__('Tournament', FakeTournamentClass)
      req = {
        body: sampleArgs,
        user: 'fake_user'
      }
    })
    it('should call utils.handleError if request body is empty', () => {
      delete req.body
      tournamentController.create(req, res)
      expect(jsonStub.called).to.be.false
      expect(statusStub.called).to.be.false
      expect(handleErrorStub.calledOnceWith(res, { message: 'missing body' }, 400)).to.be.true
      expect(FakeTournamentClass.called).to.be.false
      expect(saveStub.called).to.be.false
    })
    it('should call utils.handleError if save yields error', () => {
      saveStub = saveStub.yields(error, null)
      saveObj.rank = 'Lokalny'
      tournamentController.create(req, res)
      expect(jsonStub.called).to.be.false
      expect(statusStub.called).to.be.false
      expect(handleErrorStub.calledOnceWith(res, error, 500)).to.be.true
      expect(FakeTournamentClass.calledOnceWith(req.body)).to.be.true
      expect(saveStub.calledOnceWith(sinon.match.func)).to.be.true
      expect(saveObj.accepted).to.be.true
    })
    it('should call status 200 and return created tournament', () => {
      saveStub = saveStub.yields(null, sampleTournament)
      saveObj.rank = 'Master'
      tournamentController.create(req, res)
      expect(jsonStub.calledOnceWith(sampleTournament)).to.be.true
      expect(statusStub.calledOnceWith(200)).to.be.true
      expect(handleErrorStub.called).to.be.false
      expect(FakeTournamentClass.calledOnceWith(req.body)).to.be.true
      expect(saveStub.calledOnceWith(sinon.match.func)).to.be.true
      expect(saveObj.accepted).to.be.false
      expect(saveObj.createDate).to.be.not.null
      expect(saveObj.creator).to.be.equal(req.user)
    })
  })

  context('update', () => {
    let updateStub
    beforeEach(() => {
      updateStub = sandbox.stub()
    })

    it('should delete id, _id and creator property from body and write properties to tournament object', () => {
      saveStub = saveStub.yields(null, sampleTournament)
      sampleTournament.save = saveStub
      req = {
        locals: {
          tournament: sampleTournament
        },
        body: {
          id: 321,
          _id: 321,
          creator: 5455
        }
      }
      tournamentController.__set__('checkPrivilage', () => true)
      updateStub = updateStub.yields(null, sampleTournament)
      tournamentController.update(req, res)
      expect(handleErrorStub.called).to.be.false
      expect(statusStub.calledOnceWith(200)).to.be.true
      expect(req.body).to.not.have.property('id')
      expect(req.body).to.not.have.property('_id')
      expect(req.body).to.not.have.property('creator')
      expect(sampleTournament.id).to.be.equal(123)
      expect(sampleTournament.creator).to.be.deep.equal({
        email: 'foo@bar',
        id: 'fake_id'
      })
      expect(sampleTournament._id).to.not.exist
    })

    it('should call handle error if save yields error', () => {
      saveStub = saveStub.yields(error, null)
      sampleTournament.save = saveStub
      req = {
        locals: {
          tournament: sampleTournament
        },
        body: {
          id: 321,
          _id: 321,
          creator: 5455
        }
      }
      tournamentController.__set__('checkPrivilage', () => true)
      updateStub = updateStub.yields(null, sampleTournament)
      tournamentController.update(req, res)
      expect(handleErrorStub.calledOnceWith(res, error, 500)).to.be.true
      expect(statusStub.called).to.be.false
      expect(jsonStub.called).to.be.false
    })

    it('should call checkPrivilage and set property', () => {
      saveStub = saveStub.yields(error, null)
      sampleTournament.save = saveStub
      req = {
        locals: {
          tournament: sampleTournament
        },
        body: {
          id: 321,
          _id: 321,
          creator: 5455,
          fake: 'prop'
        }
      }
      const checkPrivilageStub = sandbox.spy()
      tournamentController.__set__('checkPrivilage', checkPrivilageStub)
      updateStub = updateStub.yields(null, sampleTournament)
      tournamentController.update(req, res)
      expect(handleErrorStub.calledOnceWith(res, error, 500)).to.be.true
      expect(statusStub.called).to.be.false
      expect(jsonStub.called).to.be.false
      expect(checkPrivilageStub.calledOnceWith(req, res, sampleTournament))
      expect(sampleTournament.fake).to.be.equal('prop')
    })
  })

  context('getWithParams', () => {
    beforeEach(() => {
      req = {
        query: 'fake_query'
      }
      tournamentController.__set__('fullPopulate', 'fakefullPopulate')
      findStub = sinon
        .mock(Tournament)
        .expects('find')
        .withArgs('fake_query')
        .chain('populate')
        .withArgs('fakefullPopulate')
    })
    afterEach(() => {
      sinon.verifyAndRestore()
    })
    it('should call handle error if query yields error', () => {
      findStub = findStub.chain('exec').withArgs(sinon.match.func).yields(error, null)
      tournamentController.getWithParams(req, res)
      expect(handleErrorStub.calledOnceWith(res, error)).to.be.true
      expect(jsonStub.called).to.be.false
      expect(statusStub.called).to.be.false
      expect(findStub.called).to.be.true
    })
  })
})
