// const mongoose = require('mongoose');
const Tournament = require('../models/tournament')
const utils = require('../lib/utils')

const fullPopulate = [
  {
    path: 'players.player',
    select: 'id email firstName lastName score bonusScore',
    model: 'User'
  },
  {
    path: 'creator',
    select: 'id email firstName lastName',
    model: 'User'
  },
  {
    path: 'rounds.matches',
    select: 'id playerA playerB finished tournament table',
    populate: [
      { path: 'playerA.player', model: 'User', select: '_id mmr firstName lastName' },
      { path: 'playerB.player', model: 'User', select: '_id mmr firstName lastName' }
    ]
  }
]

const checkPrivilage = (req, res, tournament) => {
  if (req.user.id !== tournament.creator.id) return utils.handleError(res, { message: 'Missing privileges' }, 401)
  return true
}

exports.create = function createTournament (req, res) {
  const { body } = req
  if (!body) return utils.handleError(res, { message: 'missing body' }, 400)

  const tournament = new Tournament(body)
  tournament.accepted = tournament.rank === 'Lokalny' || tournament.rank === 'Liga'
  tournament.creator = req.user
  tournament.createDate = Date.now()

  return tournament.save((err, saved) => {
    if (err) return utils.handleError(res, err, 500)
    return res.status(200).json(saved)
  })
}

exports.findTournamentForMatch = (req, res, next) => Tournament
  .findById(req.body.tournamentId)
  .select('creator name id matches players')
  .populate([
    {
      path: 'creator',
      select: 'id email firstname lastName',
      model: 'User'
    },
    {
      path: 'rounds.matches',
      select: '_id playerA playerB finished',
      populate: [
        { path: 'playerA.player', model: 'User', select: '_id mmr firstName lastName score' },
        { path: 'playerB.player', model: 'User', select: '_id mmr firstName lastName score' }
      ]
    },
    {
      path: 'players.player',
      select: 'id score',
      model: 'User'
    }
  ])
  .exec((err, tournament) => {
    if (err) return utils.handleError(res, new Error('fake error'), 500)
    if (!tournament) return utils.handleError(res, new Error('tournament not found'))
    if (tournament.creator.id !== req.user.id) return utils.handleError(res, new Error('Forbidden'), 403)
    if (!res.locals) res.locals = {}
    res.locals.tournament = tournament
    return next()
  })

exports.update = function updateTournament (req, res) {
  const { tournament } = req.locals
  delete req.body.id
  delete req.body._id
  delete req.body.creator
  checkPrivilage(req, res, tournament)
  Object.keys(req.body).forEach((key) => {
    tournament[key] = req.body[key]
  })
  return tournament.save((error, saved) => {
    if (error) return utils.handleError(res, error, 500)
    return res.status(200).json(saved)
  })
}

exports.get = function getTournamentById (id, callback) {
  if (!id) return callback(new Error('Invalid id'), null)

  return Tournament.findById(id, (err, result) => {
    if (err) return callback(err)
    return callback(null, result)
  })
}

exports.getWithParams = function getTournament (req, res) {
  return Tournament
    .find(req.query)
    .populate(fullPopulate)
    .exec((err, tournaments) => {
      if (err || !tournaments) return utils.handleError(res, err)
      tournaments.forEach((tournament) => {
        tournament.players.sort((a, b) => (a.score + a.bonusScore) < (b.score + b.bonusScore))
      })
      return res.status(200).json(tournaments)
    })
}

exports.delete = function deleteTournament (req, res) {
  const { tournament } = req.locals
  if (req.user.id !== tournament.creator.id) return utils.handleError(res, { message: 'Missing privileges' }, 401)
  if (tournament.finished) return utils.handleError(res, { message: 'cannot delete finished tournament' })
  return Tournament.findByIdAndDelete(req.params.id, (error, result) => {
    if (error) return utils.handleError(res, error)
    return res.status(200).json(result)
  })
}

exports.findOne = function findOneTournament (req, res, next) {
  return Tournament
    .findById(req.params.id)
    .populate(fullPopulate)
    .exec((err, tournament) => {
      if (err || !tournament) return utils.handleError(res, err)
      if (!req.locals) req.locals = {}
      req.locals.tournament = tournament
      return next()
    })
}

exports.startFirstRound = function startFirstRound (req, res) {
  const { tournament } = req.locals
  if (tournament.finished) return res.status(200).json({ message: 'tournament already finished' })
  if (tournament.creator.id !== req.user.id) return utils.handleError(res, { message: 'Forbidden' }, 403)
  if (tournament.rounds.length >= 1) return utils.handleError(res, new Error('Cannot start first round when second round is in play'))
  return tournament.createFirstRound((error, result) => {
    if (error) return utils.handleError(res, error, 500)
    return res.status(200).json(result.rounds)
  })
}

exports.finishRound = function finishActiveRound (req, res) {
  const { tournament } = req.locals
  if (req.user.id !== tournament.creator.id) return utils.handleError(res, new Error('Forbidden'), 403)
  const round = tournament.rounds.find(v => v.turn.toString() === req.params.round)
  if (!round) return utils.handleError(res, new Error('Round not exists'))
  if (round.finished) return utils.handleError(res, new Error('Round already finished'))
  return tournament.finishRound(round, (error, result) => {
    if (error) return utils.handleError(res, error, 500)
    return res.status(200).json({ message: `finished round ${req.params.round}`, tournamentId: result.id })
  })
}

exports.nextRound = function startNextRound (req, res) {
  const { tournament } = req.locals
  if (tournament.creator.id !== req.user.id) return utils.handleError(res, new Error('Forbidden'), 403)
  if (tournament.finished) return utils.handleError(res, new Error('Tournament already finished'))
  if (tournament.rounds.length >= tournament.maxRounds) return utils.handleError(res, new Error('Tournament maximum rounds already reached'))
  return tournament.createRound((error, result) => {
    if (error) return utils.handleError(res, error, 500)
    return res.status(200).json(result)
  })
}

exports.register = function registerUserToTournament (req, res) {
  const { tournament } = req.locals
  if (tournament.finished) return res.status(200).json({ message: 'Turniej został juz zakończony' })
  if (tournament.players.find(p => p.player.id === req.user.id)) return utils.handleError(res, { message: 'Gracz jest juz zarejestrowany' })
  if (tournament.players.length >= tournament.maxPlayers) return res.status(200).json({ message: 'Brak wolnych miejsc. Nie mozna zarejestrowac' })
  if (tournament.rounds.length > 0) return utils.handleError(res, { message: 'Nie mozna dołączyć do turnieju w trakcie gry.' })
  return tournament.registerUser(req.user, req.body, (error) => {
    if (error) return utils.handleError(res, { message: 'Błąd podczas rejestrowania.' }, 500)
    return res.status(200).json({ message: 'Dodano do turnieju' })
  })
}

exports.unregisterPlayer = function unregisterPlayerFromTournament (req, res) {
  const { tournament } = req.locals
  if (tournament.finished) return res.status(200).json({ message: 'Nie mozna wyrejestrowac z zakonczonego turnieju.' })
  if (tournament.rounds > 0) return res.status(200).json({ message: 'Nie mozna wyrejestrować w trakcie trwania turnieju' })
  return tournament.unregister(req.user, (error, result) => {
    if (error) throw error
    return res.status(200).json(result)
  })
}

exports.finishTournament = function finishTournament (req, res) {
  const { tournament } = req.locals
  if (tournament.finished) return res.status(200).json({ message: 'tournament already finished' })
  return tournament.finish((error, result) => {
    if (error) return utils.handleError(res, error, 500)
    return res.status(200).json(result)
  })
}

exports.getResults = function getActualResults (req, res) {
  const { tournament } = req.locals
  tournament.players.sort((a, b) => (a.score < b.score ? 1 : -1))
  return res.status(200).json({ results: tournament.players })
}

exports.addScoreToPlayer = function addBonusScoreToPlayer (req, res) {
  const { tournament } = req.locals
  const { score } = req.body
  if (tournament.creator.id !== req.user.id) return utils.handleError(res, new Error('Forbidden'), 403)
  return tournament.addScore(req.params.playerId, score, (error, result) => {
    if (error) return utils.handleError(res, error, 500)
    return res.status(200).json(result)
  })
}
