const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const session = require('express-session');
const mongoose = require('mongoose');
const passport = require('passport');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/api/users');
const tournamentRouter = require('./routes/api/tournament');
const matchRouter = require('./routes/api/match');
const usersPageRouter = require('./routes/users_page');
const tournamentPageRouter = require('./routes/tournament_page');
const rankingRouter = require('./routes/api/ranking');

require('./models/user');
require('./models/tournament');
require('./models/match');

require('./config/passport');

const app = express();

const { MONGODB_USER, MONGODB_PASSWORD, MONGODB_DATABASE } = process.env;
const connectionAzureString = `mongodb://${MONGODB_USER}:${MONGODB_PASSWORD}@127.0.0.1:3000/${MONGODB_DATABASE}`;
// mongoose.connect((process.env.env === 'production' ? connectionAzureString : 'mongodb://localhost:27017/test'), { useNewUrlParser: true });
// mongoose.set('debug', true);
// mongoose.connect('mongodb+srv://tournamentapp:RVHo4mgJ97EeUOVl@cluster0-omquk.azure.mongodb.net/test?retryWrites=true');
const uri = "mongodb+srv://tournamentapp:wr8hEaE5X56spTQB@cluster0-omquk.azure.mongodb.net/test?retryWrites=true";
//const client = new MongoClient(uri, { useNewUrlParser: true });
mongoose.connect(uri, { useNewUrlParser: true }, err => {
  console.log(err);
});
mongoose.Promise = global.Promise;
// Get the default connection
const db = mongoose.connection;

// // Bind connection to error event (to get notification of connection errors)
// eslint-disable-next-line no-console
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({
  extended: true,
}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public/build')));
app.use(session({
  secret: 'passport-tutorial',
  cookie: { maxAge: 60000 },
  resave: false,
  saveUninitialized: false,
}));

app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  res.locals.user = req.user;
  next();
});

app.use('/', indexRouter);
app.use('/api/user', usersRouter);
app.use('/users', usersPageRouter);
app.use('/api/tournament', tournamentRouter);
app.use('/tournament', tournamentPageRouter);
app.use('/api/match', matchRouter);
app.use('/api/ranking', rankingRouter);


// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404, new Error('page not found')));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
